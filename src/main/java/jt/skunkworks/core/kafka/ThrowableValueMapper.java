package jt.skunkworks.core.kafka;

import org.apache.kafka.streams.kstream.ValueMapper;

@FunctionalInterface
public interface ThrowableValueMapper<K, V> extends ValueMapper<K, V> {

    @Override
    default V apply(K k) {
        try {
            return applyThrowable(k);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    V applyThrowable(K k) throws Exception;
}

