package jt.skunkworks.core.kafka;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.state.HostInfo;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.springframework.cloud.stream.binder.kafka.streams.InteractiveQueryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;
import java.util.function.Function;

// TODO - Convert to kotlin
public class InteractiveQueryDelegate<T> {

    private InteractiveQueryService queryService;
    private RestTemplate restTemplate;
    private Class<T> clazz;
    private String storeName;

    public InteractiveQueryDelegate(String storeName,
                                    InteractiveQueryService queryService,
                                    RestTemplate restTemplate,
                                    Class<T> clazz) {
        this.storeName = storeName;
        this.queryService = queryService;
        this.restTemplate = restTemplate;
        this.clazz = clazz;
    }

    public T get(String id, Function<HostInfo, String> urlBuilder) {
        HostInfo hostInfo = queryService.getHostInfo(storeName, id, Serdes.String().serializer());

        if ("unavailable".equalsIgnoreCase(hostInfo.host())
        || queryService.getCurrentHostInfo().equals(hostInfo)) {
            ReadOnlyKeyValueStore<String, T> queryableStore = queryService.getQueryableStore(storeName,
                    QueryableStoreTypes.keyValueStore());
            return queryableStore.get(id);
        } else {
            ResponseEntity<T> resp = restTemplate.getForEntity(urlBuilder.apply(hostInfo), clazz);
            return Optional.ofNullable(resp.getBody())
                    .orElse(null);
        }
    }
}
