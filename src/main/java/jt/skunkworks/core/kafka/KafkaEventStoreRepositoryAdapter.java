package jt.skunkworks.core.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import jt.skunkworks.core.domain.BaseAggregate;
import jt.skunkworks.core.domain.BaseEvent;
import jt.skunkworks.core.event.EventStoreRepository;
import org.apache.kafka.streams.state.HostInfo;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import java.util.List;
import java.util.function.Function;

import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;

// TODO Document Me
public class KafkaEventStoreRepositoryAdapter<T> implements EventStoreRepository<T> {

    private InteractiveQueryDelegate<T> delegate;
    private MessageChannel messageChannel;
    @Value("${app.protocol:http}")
    private String protocol;

    // TODO clean up
    private ObjectMapper objectMapper;

    protected KafkaEventStoreRepositoryAdapter(
            InteractiveQueryDelegate<T> delegate,
            MessageChannel messageChannel
    ) {
        this.delegate = delegate;
        this.messageChannel = messageChannel;
        objectMapper  = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule())
                .enable(SerializationFeature.INDENT_OUTPUT)
                .disable(WRITE_DATES_AS_TIMESTAMPS);

    }

//    @Autowired
//    public void setObjectMapper(ObjectMapper objectMapper) {
//        this.objectMapper = objectMapper;
//    }

    @Nullable
    public T getSnapshot(@NotNull String aggregateId, String postfix) {
        return delegate.get(aggregateId, alternateUrl(postfix));
    }

    private Function<HostInfo, String> alternateUrl(String postfix) {
        return hostInfo ->
                format("%s://%s:%d/%s",
                        protocol,
                        hostInfo.host(),
                        hostInfo.port(),
                        postfix);
    }

    @Nullable
    @Override
    public final T getSnapshot(@NotNull String aggregateId) {
        return getSnapshot(aggregateId, aggregateId);
    }

    @Override
    public <A extends BaseAggregate> void saveSnapshot(@NotNull String aggregateId, @NotNull A aggregate) {}

    @NotNull
    @Override
    public List<BaseEvent> getEvents(@NotNull String aggregateId, int versionTill) {
        return emptyList();
    }

    @Override
    public void save(@NotNull BaseEvent event) {
        String payload = null;
        try {
            payload = objectMapper.writeValueAsString(event);
            System.out.println(payload);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        messageChannel
                .send(MessageBuilder
                        //.withPayload(event)
                        .withPayload(payload)
                        .setHeader("aggregateId", requireNonNull(event.getAggregateId()))
                        .build());

    }
}
