package jt.skunkworks.core.advice

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.badRequest
import org.springframework.http.ResponseEntity.status
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class GlobalErrorHandler {
    private val log = LoggerFactory.getLogger(GlobalErrorHandler::class.java)

    @ExceptionHandler(AssertionError::class)
    fun onAssertionError(t: AssertionError): ResponseEntity<String> {
        return badRequest().body(t.message)
    }

    @ExceptionHandler(RuntimeException::class)
    fun onException(t: Exception): ResponseEntity<String> {
        log.error("[EXCEPTION] Unexpected Error", t);
        return status(INTERNAL_SERVER_ERROR).body(t.message)
    }
}