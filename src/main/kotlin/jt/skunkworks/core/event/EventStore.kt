package jt.skunkworks.core.event

import jt.skunkworks.core.domain.BaseAggregate
import jt.skunkworks.core.domain.BaseEvent
import org.slf4j.LoggerFactory


open class EventStore (
        private val repository: EventStoreRepository<out BaseAggregate>,
        // TODO Document Me
        // It is optional, if the EventStoreRepository provides the publish effect. For example, Kafka stream
        private val publisher: EventStorePublisher? = null,
        // TODO Can this be remove?
        private val initializer: () -> BaseAggregate
        ) {
    private val log = LoggerFactory.getLogger(EventStore::class.java)
    open fun getAggregate(rootId: String): BaseAggregate {
        val lastSnapshot = repository.getSnapshot(rootId) ?: initializer.invoke()
        val events = repository.getEvents(aggregateId = rootId, versionTill = lastSnapshot.version)
        if (events.isNotEmpty()) {
            lastSnapshot.version = lastSnapshot.version + events.size
//            println("!!! SAVING snapshot.version: ${lastSnapshot.version}")
            lastSnapshot.replay(events)
            repository.saveSnapshot(rootId, lastSnapshot)
            log.debug("!!! snapshot saved aggregateId: $rootId, version: ${lastSnapshot.version}")
        }
        log.debug("!!! snapshot retrieved aggregateId: $rootId, version: ${lastSnapshot.version}")
        return lastSnapshot
    }

    open fun saveAndPublish(rootId: String, event: BaseEvent) {
        // Optimistic Concurrency Control
        repository.getSnapshot(rootId)?.let{
            log.debug("!!! event version: ${event.version} vs aggregate version: ${it.version}")
            assert(event.version == it.version + 1) { "Version mismatch ${event.version} vs ${it.version + 1}" }
        }
        repository.save(event)
        publisher?.emit(event)
    }
}
