package jt.skunkworks.core.event

import jt.skunkworks.core.domain.BaseAggregate
import jt.skunkworks.core.domain.BaseEvent

interface EventStoreRepository <T> {
    fun getSnapshot(aggregateId: String): T?

    fun <A: BaseAggregate> saveSnapshot(aggregateId: String, aggregate: A)

    fun getEvents(aggregateId: String, versionTill: Int): List<BaseEvent>

    fun save(event: BaseEvent)

}