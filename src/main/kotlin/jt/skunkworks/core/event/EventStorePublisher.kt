package jt.skunkworks.core.event

import jt.skunkworks.core.domain.BaseEvent

interface EventStorePublisher {
    fun emit(event: BaseEvent)
}
