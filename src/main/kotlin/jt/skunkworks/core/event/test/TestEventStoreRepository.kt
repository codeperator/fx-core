package jt.skunkworks.core.event.test

import jt.skunkworks.core.domain.BaseAggregate
import jt.skunkworks.core.domain.BaseEvent
import jt.skunkworks.core.event.EventStoreRepository
import org.slf4j.LoggerFactory

class TestEventStoreRepository: EventStoreRepository<BaseAggregate> {

    private val snapshot = mutableMapOf<String, BaseAggregate>()
    private val events = mutableMapOf<String, MutableList<BaseEvent>>()
    private val log = LoggerFactory.getLogger(TestEventStoreRepository::class.java)

    override fun getSnapshot(aggregateId: String): BaseAggregate? {
        val origin = snapshot[aggregateId]
        return origin
    }

    override fun <A : BaseAggregate> saveSnapshot(aggregateId: String, aggregate: A) {
        log.debug("!!! saveSnapshot, aggregateId: $aggregateId, version ${aggregate.version}")
        snapshot[aggregateId] = aggregate
    }

    override fun getEvents(aggregateId: String, versionTill: Int): List<BaseEvent> {
        return events.computeIfAbsent(aggregateId) { mutableListOf() }
                .filter { e -> e.version >= versionTill }
    }

    override fun save(event: BaseEvent) {
        events.computeIfAbsent(event.aggregateId!!) { mutableListOf(event) }
                .add(event)
    }
}
