package jt.skunkworks.core.event.test

import jt.skunkworks.core.event.EventAdvice
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.EnableAspectJAutoProxy
import org.springframework.context.annotation.Import


class TestEventConfig {
    @Bean
    fun reporsitory(): TestEventStoreRepository = TestEventStoreRepository()

    @Bean
    fun publisher(): TestEventPublisher = TestEventPublisher()

    @Bean
    fun advice(applicationContext: ApplicationContext) = EventAdvice(applicationContext)
}

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(TestEventConfig::class)
@EnableAspectJAutoProxy
annotation class ImportTestEventConfig


