package jt.skunkworks.core.event.test

import jt.skunkworks.core.domain.BaseEvent
import jt.skunkworks.core.event.EventStorePublisher

class TestEventPublisher(
        private val eventCollector: MutableMap<String, MutableList<BaseEvent>> = mutableMapOf()
): EventStorePublisher {

    override fun emit(event: BaseEvent) {
        println("!!! Event publish ${event.aggregateId} ${event.javaClass.simpleName}")
        eventCollector.computeIfAbsent(event.aggregateId!!)
        { mutableListOf() }
                .add(event)
    }

    fun get(aggregateId: String) = eventCollector.getOrElse(aggregateId) { mutableListOf() }

}
