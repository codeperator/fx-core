package jt.skunkworks.core.event

import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.EnableAspectJAutoProxy

@EnableAspectJAutoProxy
@Configuration
class EventAdviceConfig {
    @Bean
    fun eventAdvice(applicationContext: ApplicationContext) = EventAdvice(applicationContext)
}