package jt.skunkworks.core.event

import jt.skunkworks.core.annotation.AggregateId
import jt.skunkworks.core.annotation.CommandHandler
import jt.skunkworks.core.annotation.EventDetail
import jt.skunkworks.core.domain.BaseAggregate
import jt.skunkworks.core.domain.BaseEvent
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.reflect.MethodSignature
import org.slf4j.LoggerFactory
import org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint
import org.springframework.context.ApplicationContext
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor
import java.lang.AssertionError as AssertionError1

@Aspect
class EventAdvice(
        private val applicationContext: ApplicationContext
) {
    private val log = LoggerFactory.getLogger(EventAdvice::class.java)

    @Around("@annotation(jt.skunkworks.core.annotation.CommandHandler)")
    fun advice(joinPoint: ProceedingJoinPoint) {
        val aggregateClass = getAggregateClass(joinPoint)
        val annotatedMethodArgument = getAnnotatedMethodArgument(joinPoint as MethodInvocationProceedingJoinPoint)
        // BEFORE
        val aggregateId = getAggregateId(annotatedMethodArgument)
                .run {
            when (this) {
                is UUID -> this.toString()
                is String -> this
                else -> throw IllegalArgumentException("AggregateId of type  ${this?.javaClass?.simpleName} is unsupported")
            }
        }

        val eventStore = getEventStore(aggregateClass)
        val aggregate = eventStore.getAggregate(aggregateId)
        log.debug("!! BEF aggregateId: $aggregateId, version: ${aggregate.version}")

        // apply Command on Aggregate
        val eventDetailMeta = annotatedMethodArgument.find { annotationValue -> annotationValue.first is EventDetail }
        aggregate.setAggregateId(aggregateId)
        executeCommand(aggregate, joinPoint, aggregateClass, eventDetailMeta)

        joinPoint.proceed()

        //- AFTER
        val eventClass = getEventClass(joinPoint)
        val event = createEvent(eventDetailMeta, eventClass)

        // copy aggregate state to event which match the name

        event!!.aggregateId = aggregateId
        event.version = aggregate.version + 1
        log.debug("!!! aggregate version ${aggregate.version}, event version ${event.version}");
        //- OnBeforeSave
        log.debug("!! AFT aggregateId: $aggregateId, event: ${event.aggregateId}")
        eventStore.saveAndPublish(aggregateId, event)
    }

    private fun getEventStore(aggregateClass: KClass<out BaseAggregate>): EventStore {
        val beans = applicationContext.getBeansOfType(EventStore::class.java)
        // TODO : Document Me : name the eventStore
        val eventStore = beans[aggregateClass.simpleName] ?: beans["eventStore"]
        assert(eventStore != null) {"""
            Unable to retrieve eventStore from spring application context 
            with bean name "eventStore" or ${aggregateClass.simpleName} 
            """}
        return eventStore!!
    }

    private fun createEvent(eventDetailMeta: Pair<Annotation, Any>?, eventClass: KClass<out BaseEvent>): BaseEvent? {
        return if (eventDetailMeta?.second != null) {
            eventClass.primaryConstructor?.call(eventDetailMeta.second)
        } else {
            eventClass.primaryConstructor?.call()
        }
    }

    private fun executeCommand(aggregate: BaseAggregate, joinPoint: MethodInvocationProceedingJoinPoint, aggregateClass: KClass<out BaseAggregate>, eventDetailMeta: Pair<Annotation, Any>?) {
        val commandMethod = aggregate.javaClass.methods.find { it.name == joinPoint.signature.name }
        assert(commandMethod != null) { "No such method '${joinPoint.signature.name}' in $aggregateClass" }
        try {
            if (eventDetailMeta != null) {
                log.debug("Invoke $aggregateClass ${commandMethod?.name}(..)")
                commandMethod?.invoke(aggregate, eventDetailMeta.second)
            } else {
                log.debug("Invoke $aggregateClass ${commandMethod?.name}()")
                commandMethod?.invoke(aggregate)
            }
        } catch (e: Exception) {
            when (e.cause) {
                is AssertionError1 -> throw e.cause as java.lang.AssertionError
                else -> throw RuntimeException(e)
            }
        }
    }

    private fun getAggregateId(annotatedMethodArgument: List<Pair<Annotation, Any>>) =
        annotatedMethodArgument
                .find { annotationValue -> annotationValue.first is AggregateId }
                ?.second

    private fun getAnnotatedMethodArgument(joinPoint: MethodInvocationProceedingJoinPoint): List<Pair<Annotation, Any>> {
        val signature = joinPoint.signature as MethodSignature
        val annotatedValueList = mutableListOf<Pair<Annotation, Any>>()
        val method = signature.method
        val annotations = signature.method.parameterAnnotations
        annotations.forEachIndexed { i, arrayOfAnnotations ->
            arrayOfAnnotations.forEachIndexed { _, annotation ->
                when (annotation) {
                    is AggregateId -> {
                        val inputValue = joinPoint.args[i]
                        log.debug("Found AggregateId @ $i $annotation $inputValue")
                        annotatedValueList.add(Pair(annotation, inputValue))

                    }
                    is EventDetail -> {
                        val inputValue = joinPoint.args[i]
                        log.debug("Found EventDetail @ $i $annotation $inputValue")
                        annotatedValueList.add(Pair(annotation, inputValue))
                    }
                    else -> {
                        log.debug("Noop $annotation")
                    }
                }
            }
        }
        val aggregateIdMeta = annotatedValueList.filter { p -> p.first is AggregateId }
        assert(aggregateIdMeta.size == 1) { "Only 1 @AggregateId annotation required for method ${method.name}" }
        val eventDetailMeta = annotatedValueList.filter { p -> p.first is EventDetail }
        assert(eventDetailMeta.size <= 1) { "Only 1 or no @EventDetail can be annotated for method ${method.name}" }
        return annotatedValueList
    }

    private fun getAggregateClass(joinPoint: ProceedingJoinPoint): KClass<out BaseAggregate> {
        fun isBase(baseAggregateClass: KClass<out BaseAggregate>) = baseAggregateClass.simpleName == BaseAggregate::class.java.simpleName
        // method level
        val methodLevel = joinPoint.methodAnnotation(CommandHandler::class.java) as CommandHandler
        val aggregateClass =
                if (isBase(methodLevel.aggregateClass)) {
                    (joinPoint.classAnnotation(CommandHandler::class.java) as CommandHandler).aggregateClass
                } else {
                    methodLevel.aggregateClass
                }
        log.info("aggregateClass: $aggregateClass")
        assert(!isBase(aggregateClass)) { "There are no aggregateClass defined for $joinPoint" }
        return aggregateClass
    }

    private fun getEventClass(joinPoint: ProceedingJoinPoint): KClass<out BaseEvent> {
        val methodLevel = joinPoint.methodAnnotation(CommandHandler::class.java) as CommandHandler
        val eventClass = methodLevel.eventClass
        assert(eventClass.simpleName != BaseEvent::class.java.simpleName) { "There are no eventClass defined for $joinPoint" }
        return eventClass

    }
}
