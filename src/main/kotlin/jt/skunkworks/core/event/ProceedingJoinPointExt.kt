package jt.skunkworks.core.event

import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.reflect.MethodSignature

fun ProceedingJoinPoint.methodAnnotation(annotationClass: Class<out Annotation>): Annotation? = (this.signature as MethodSignature)
        .method
        .getAnnotation(annotationClass)

fun ProceedingJoinPoint.classAnnotation(annotationClass: Class<out Annotation>): Annotation? = this.target
        .javaClass
        .getAnnotation(annotationClass)
