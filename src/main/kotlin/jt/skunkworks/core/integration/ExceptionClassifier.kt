package jt.skunkworks.core.integration

import org.springframework.classify.SubclassClassifier
import org.springframework.retry.RetryPolicy
import org.springframework.retry.policy.NeverRetryPolicy

class ExceptionClassifier(
        val typeMap: MutableMap<Class<out Throwable>, RetryPolicy>
) : SubclassClassifier<Throwable, RetryPolicy>(typeMap, NeverRetryPolicy()) {

    override fun classify(classifiable: Throwable): RetryPolicy? {
        val typeMap = classified
        var cause: Throwable? = classifiable
        var causeClass: Class<*> = classifiable.javaClass
        do {
            if (typeMap.containsKey(causeClass)) {
                return typeMap[causeClass]
            } else {
                // search by inheritance
                val classify = super.classify(cause)
                if (classify !== super.getDefault()) {
                    return classify
                }
            }
            // traverse to the next cause
            cause = cause!!.cause
            if (cause != null) {
                causeClass = cause.javaClass
            }
        } while (cause != null)
        return default
    }
}
