package jt.skunkworks.core.domain

abstract class BaseAggregate {
    var version: Int = 0
    abstract fun getAggregateId(): String?
    abstract fun setAggregateId(id: String)
    abstract fun handle(event: BaseEvent): BaseAggregate
    fun replay(events: List<BaseEvent>) {
        events.forEach { handle(it)}
    }
}
