package jt.skunkworks.core.domain

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonTypeInfo
import java.time.LocalDateTime
import java.time.OffsetDateTime

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
abstract class BaseEvent (
        var aggregateId: String? = null,
        var version: Int = 0,
        // TODO custom annotation to contain the format
        @get:JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssXX", shape = JsonFormat.Shape.STRING)
        var timestamp: OffsetDateTime = OffsetDateTime.now()
)