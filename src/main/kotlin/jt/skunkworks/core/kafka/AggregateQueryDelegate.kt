package jt.skunkworks.core.kafka

import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.state.HostInfo
import org.apache.kafka.streams.state.QueryableStoreTypes
import org.springframework.cloud.stream.binder.kafka.streams.InteractiveQueryService
import org.springframework.web.client.RestTemplate
import java.util.*

class KAggregateQueryDelegate (
        val storeName: String,
        val queryService: InteractiveQueryService,
        val restTemplate: RestTemplate
) {

    inline fun <reified T> get(id: String, urlBuilder: (HostInfo) -> String): T {
        // get meta data
        val hostInfo = queryService.getHostInfo(storeName, id, Serdes.String().serializer())
        // does this instance host the partitioned record
        if (queryService.getCurrentHostInfo() == hostInfo) {
            // YES: REST call this host for data
            // get the Kafka store
            // Kafka store.get(Key): the key is the aggregateId to return AccountAggregate
            val queryableStore = queryService.getQueryableStore(storeName,
                    QueryableStoreTypes.keyValueStore<String, T>())
            return queryableStore.get(id)
        } else {
            // NO:  REST call the other host for data
            val resp = restTemplate.getForEntity(urlBuilder.invoke(hostInfo), T::class.java, id)
            return Optional.ofNullable<T>(resp.getBody())
                    .orElse(null)
        }

    }
}