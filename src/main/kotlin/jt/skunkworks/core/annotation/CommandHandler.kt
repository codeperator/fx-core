package jt.skunkworks.core.annotation

import jt.skunkworks.core.domain.BaseAggregate
import jt.skunkworks.core.domain.BaseEvent
import kotlin.reflect.KClass

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class CommandHandler(
        /*
        Defines aggregate class apply either on method or class
         */
        val aggregateClass: KClass<out BaseAggregate> = BaseAggregate::class,

        /*
        Defines event class the  method should emit
         */
        val eventClass: KClass<out BaseEvent> = BaseEvent::class
)
