package jt.skunkworks.core.event.test

import jt.skunkworks.core.sample.SampleActivatedEvent
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

internal class TestEventPublisherTest{

    @Test
    fun `Emitted event can be retrieve for assertion`() {
        val publisher = TestEventPublisher()
        val event = SampleActivatedEvent()
        event.aggregateId = "TEST-ID"
        publisher.emit(event)
        assertEquals(event, publisher.get(event.aggregateId!!)[0])
    }
}

