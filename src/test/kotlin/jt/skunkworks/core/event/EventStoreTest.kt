package jt.skunkworks.core.event

import com.nhaarman.mockito_kotlin.*
import jt.skunkworks.core.sample.CreateCommand
import jt.skunkworks.core.sample.SampleAggregate
import jt.skunkworks.core.sample.SampleUpdatedEvent
import jt.skunkworks.core.sample.UpdateCommand
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`


internal class EventStoreTest {

    private val repository: EventStoreRepository<SampleAggregate> = mock()
    private val publisher: EventStorePublisher = mock()
    private val eventStore: EventStore = EventStore(repository, publisher) { SampleAggregate() }

    @Test
    fun `getAggregate for the first time`() {
        `when`(repository.getSnapshot(any())).thenReturn(null)
        val aggregate = eventStore.getAggregate("TEST-ID")
        println(aggregate)
    }

    @Test
    fun `getAggregate with snapshot and no event`() {
        val sample = SampleAggregate()
        sample.create(CreateCommand("Hello", "Secret"))
        sample.version = 1
        `when`(repository.getSnapshot(any())).thenReturn(sample)

        val aggregate = eventStore.getAggregate("TEST-ID")

        println((aggregate as SampleAggregate).message)
    }

    @Test
    fun `getAggregate with snapshot and event`() {
        val sample = SampleAggregate()
        sample.create(CreateCommand("Hello", "Secret"))
        sample.version = 1
        `when`(repository.getSnapshot(any())).thenReturn(sample)
        `when`(repository.getEvents(any(), eq(1))).thenReturn(
                listOf(SampleUpdatedEvent(UpdateCommand("Great", "I Know")))
        )

        val aggregate = eventStore.getAggregate("TEST-ID")

        println((aggregate as SampleAggregate).message)
    }

    @Test
    fun `saveAndPublish successfully for create`() {
        val rootId = "TEST-ID"

        val updatedEvent = SampleUpdatedEvent(UpdateCommand("Great", "I Know"))
                .apply { this.version = 1 }

        eventStore.saveAndPublish(rootId, updatedEvent)

        verify(repository).save(updatedEvent)
        verify(publisher).emit(updatedEvent)
    }

    @Test
    fun `saveAndPublish successfully for update`() {
        val rootId = "TEST-ID"
        val sample = SampleAggregate()
        sample.create(CreateCommand("Hello", "Secret"))
        sample.version = 1
        `when`(repository.getSnapshot(any())).thenReturn(sample)

        val updatedEvent = SampleUpdatedEvent(UpdateCommand("Great", "I Know"))
                .apply { this.version = 2 }

        eventStore.saveAndPublish(rootId, updatedEvent)

        verify(repository).save(updatedEvent)
        verify(publisher).emit(updatedEvent)
    }

    @Test
    fun `saveAndPublish fail due to version mismatch`() {
        val rootId = "TEST-ID"
        val sample = SampleAggregate()
        sample.create(CreateCommand("Hello", "Secret"))
        sample.version = 1
        `when`(repository.getSnapshot(any())).thenReturn(sample)

        val updatedEvent = SampleUpdatedEvent(UpdateCommand("Great", "I Know"))
                .apply { this.version = 1 }

        val exception = assertThrows(AssertionError::class.java) {
            eventStore.saveAndPublish(rootId, updatedEvent)
        }
        assertEquals("Version mismatch 1 vs 2", exception.message)
        verify(repository, never()).save(updatedEvent)
        verify(publisher, never()).emit(updatedEvent)
    }

}
