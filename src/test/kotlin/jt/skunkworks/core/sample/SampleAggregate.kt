package jt.skunkworks.core.sample

import jt.skunkworks.core.domain.BaseAggregate
import jt.skunkworks.core.domain.BaseEvent
import org.slf4j.LoggerFactory
import java.util.*

data class SampleAggregate(var sampleId: UUID? = null,
                      var message: String? = null,
                      var secret: String? = null,
                      var state: SampleState = SampleState.CREATED,
                      var purpose: String ? =null
) : BaseAggregate() {
    private val log = LoggerFactory.getLogger(SampleAggregate::class.java)

    override fun setAggregateId(id: String) {
        this.sampleId = UUID.fromString(id)
    }

    override fun getAggregateId(): String? {
        log.info("SampleAggregate: getAggregateId")
        return sampleId.toString()
    }

    fun create(command: CreateCommand) {
        log.info("SampleAggregate: create")
        message = command.message
        secret = command.secret
        state = SampleState.PENDING
    }

    fun update(command: UpdateCommand) {
        log.info("SampleAggregate: update")
        message = command.message
        secret = command.secret
    }

    fun activate() {
        assert(state == SampleState.PENDING) {"Cannot activate with status $state"}
        log.info("SampleAggregate: activate")
        state = SampleState.ACTIVATE
    }

    override fun handle(event: BaseEvent): SampleAggregate {
        when(event) {
            is SampleCreatedEvent -> create( event.command )
            is SampleUpdatedEvent -> update( event.command )
            is SampleActivatedEvent -> activate()
        }
        return this
    }

}