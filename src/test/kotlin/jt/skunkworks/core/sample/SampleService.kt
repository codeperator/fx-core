package jt.skunkworks.core.sample

import jt.skunkworks.core.annotation.AggregateId
import jt.skunkworks.core.annotation.CommandHandler
import jt.skunkworks.core.annotation.EventDetail
import org.springframework.stereotype.Service
import java.util.*

@Service
@CommandHandler(aggregateClass = SampleAggregate::class)
class SampleService {

    // This should fail
    @CommandHandler
    fun create(@EventDetail command: CreateCommand) {
        println("create :: call $command")
    }

    @CommandHandler(eventClass = SampleCreatedEvent::class)
    fun create(@AggregateId sampleId: UUID, @EventDetail command: CreateCommand) {
        println("create :: call $command")
    }

    @CommandHandler(eventClass = SampleUpdatedEvent::class)
    fun update(@AggregateId sampleId: UUID, @EventDetail command: UpdateCommand) {
        println("update :: call $sampleId, $command")
    }

    @CommandHandler(eventClass = SampleActivatedEvent::class)
    fun activate(@AggregateId aggregateId: UUID) {
        println("activate :: call $aggregateId")
    }
}


