package jt.skunkworks.core.sample

enum class SampleState {
    CREATED, PENDING, ACTIVATE
}