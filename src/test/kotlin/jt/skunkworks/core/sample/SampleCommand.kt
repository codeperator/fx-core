package jt.skunkworks.core.sample

data class CreateCommand(val message: String, val secret: String)

data class UpdateCommand(val message: String, val secret: String)

data class ActivateCommand(val message: String)
