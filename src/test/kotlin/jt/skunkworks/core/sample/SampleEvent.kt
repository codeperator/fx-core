package jt.skunkworks.core.sample

import jt.skunkworks.core.domain.BaseEvent

sealed class SampleEvent: BaseEvent()

class SampleCreatedEvent(val command: CreateCommand): SampleEvent()

class SampleUpdatedEvent(val command: UpdateCommand): SampleEvent()

class SampleActivatedEvent: SampleEvent() {
    var purpose: String? = null
}