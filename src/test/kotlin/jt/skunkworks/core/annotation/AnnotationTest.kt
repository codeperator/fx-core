package jt.skunkworks.core.annotation

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import jt.skunkworks.core.event.EventStore
import jt.skunkworks.core.sample.CreateCommand
import jt.skunkworks.core.sample.SampleAggregate
import jt.skunkworks.core.sample.SampleService
import jt.skunkworks.core.sample.UpdateCommand
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig
import java.util.*
import kotlin.test.assertEquals

@SpringJUnitConfig(classes = [AnnotationTestConfig::class])
class AnnotationTest {

    @Autowired
    lateinit var service: SampleService
    @Autowired
    lateinit var evenStore: EventStore

    @BeforeEach
    fun setup() {
        whenever(evenStore.getAggregate(any())).thenReturn(SampleAggregate())
    }

    @Test
    fun `Annotate @CommandHandler without @AggregateId should fail `() {
        val command = CreateCommand("World", "Secret")
        val error = assertThrows(AssertionError::class.java) {
            service.create(command)
        }

        assertEquals("Only 1 @AggregateId annotation required for method create", error.message)
                error.message
    }

    @Test
    fun `Annotate @CommandHandler with @AggregateId should succeed`() {
        val command = CreateCommand("World", "Secret")
        service.create(UUID.randomUUID(), command)
        service.update(UUID.randomUUID(), UpdateCommand("World", "Secret"))
    }

    @Test
    fun `Annotate @CommandHandler execution will propagate Aggregate method error`() {
        val sampleId = UUID.randomUUID()
        val command = CreateCommand("World", "Secret")

        val error = assertThrows(AssertionError::class.java) {
            service.activate(sampleId)
        }
        assertEquals("Cannot activate with status CREATED", error.message)
        service.create(sampleId, command)
        service.activate(sampleId)
    }

}

@EnableAutoConfiguration
class AnnotationTestConfig {
    @Bean
    fun eventStore(): EventStore = mock()

    @Bean
    fun sampleService() = SampleService()
}