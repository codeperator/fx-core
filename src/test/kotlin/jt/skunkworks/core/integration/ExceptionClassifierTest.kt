package jt.skunkworks.core.integration

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.messaging.MessagingException
import org.springframework.retry.RetryPolicy
import org.springframework.retry.policy.SimpleRetryPolicy
import java.io.IOException
import java.net.UnknownHostException

class ExceptionClassifierTest {
    private val simple: RetryPolicy = SimpleRetryPolicy(3)

    @Test
    @DisplayName("Match by cause")
    fun test_cause() {
        val classifier = ExceptionClassifier(mutableMapOf(UnknownHostException::class.java to simple))
        val classify = classifier.classify(MessagingException("Things happens", UnknownHostException("Test")))
        assertEquals(simple, classify)
    }

    @Test
    @DisplayName("Match by inheritance")
    fun test_inheritance() {
        val classifier = ExceptionClassifier(mutableMapOf(IOException::class.java to simple))
        val classify = classifier.classify(MessagingException("Things happens", UnknownHostException("Test")))
        assertEquals(simple, classify)
    }

    @Test
    @DisplayName("No Match")
    fun test_no_match() {
        val classifier = ExceptionClassifier(mutableMapOf(IOException::class.java to simple))
        val classify = classifier.classify(MessagingException("Things happens", NullPointerException("Test")))
        assertNotEquals(simple, classify)
    }

}