package jt.skunkworks.core.kafka;

import org.apache.kafka.streams.state.HostInfo;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.cloud.stream.binder.kafka.streams.InteractiveQueryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;
import java.util.function.Function;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class InteractiveQueryDelegateTest {

    @Mock
    private InteractiveQueryService queryService;
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private Function<HostInfo, String> urlHandler;
    @Mock
    private ReadOnlyKeyValueStore<String, String> mockStore;
    @Captor
    private ArgumentCaptor<String> url;
    private InteractiveQueryDelegate<String> delegate;

    private final HostInfo current = new HostInfo("local", 9999);
    private final HostInfo other = new HostInfo("other", 7777);

    @BeforeEach
    void setup() {
        delegate = new InteractiveQueryDelegate<>("TEST", queryService, restTemplate, String.class);
    }

    @Test
    @DisplayName("Query execute locally")
    void query_execute_local() {
        when(queryService.getHostInfo(eq("TEST"), eq("id"), any())).thenReturn(current);
        when(queryService.getCurrentHostInfo()).thenReturn(current);
        when(queryService.getQueryableStore(eq("TEST"), any())).thenReturn(mockStore);
        when(mockStore.get("id")).thenReturn("RESULT-LOCAL");

        String result = delegate.get("id", urlHandler);
        Assertions.assertEquals("RESULT-LOCAL", result);
    }

    @Test
    @DisplayName("Query execute other")
    void query_execute_other() {
        when(queryService.getHostInfo(eq("TEST"), eq("id"), any())).thenReturn(other);
        when(queryService.getCurrentHostInfo()).thenReturn(current);
        when(restTemplate.getForEntity(url.capture(), eq(String.class)))
                .thenReturn(ResponseEntity.of(Optional.of("RESULT-OTHER")));

        String result = delegate.get("id", urlHandler);
        Assertions.assertEquals("RESULT-OTHER", result);
    }

}