import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
//    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    kotlin("jvm") version "1.4.0"
    kotlin("plugin.spring") version "1.4.0"
    id("maven-publish")
}

group = "jt.skunkworks.fx"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_14

val jupiterVer by extra( "5.5.2" )
val mockitoVer by extra( "3.2.0" )
val kotlinVer by extra( "1.4.0" )
val mockitoKotlinVer by extra( "1.6.0" )
val springCloudVersion by extra("Hoxton.SR8")

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.aspectj:aspectjweaver:1.9.6")
    implementation("org.springframework:spring-web:5.2.8.RELEASE")
    implementation("org.springframework.cloud:spring-cloud-stream-binder-kafka-streams:3.0.8.RELEASE")
//    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.11.2")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.11.2")

    implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlinVer")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlinVer")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVer")
    implementation("org.jetbrains.kotlin:kotlin-stdlib:$kotlinVer")
    testImplementation ("org.jetbrains.kotlin:kotlin-test")


//    testImplementation("org.springframework.boot:spring-boot-starter-test") {
//        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
//    }
    testImplementation("org.springframework:spring-test:5.2.8.RELEASE")
    testImplementation ("org.junit.jupiter:junit-jupiter:$jupiterVer")
    testImplementation ("org.mockito:mockito-junit-jupiter:$mockitoVer")
    testImplementation ("org.mockito:mockito-core:$mockitoVer")
    testImplementation ("com.nhaarman:mockito-kotlin:$mockitoKotlinVer")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "14"
    }
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_14
}

//dependencyManagement {
//    imports {
//        mavenBom("org.springframework.cloud:spring-cloud-dependencies:$springCloudVersion")
//        mavenBom("io.spring.platform:platform-bom:Cairo-SR8") {
//            bomProperty("kotlin.version", "1.4.0")
//            bomProperty("spring.version", "5.2.8.RELEASE")
//            bomProperty("junit-jupiter.version", jupiterVer)
//        }
//    }
//}

java {
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
        }
    }
}
